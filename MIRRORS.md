QPMS source code mirrors
========================

QPMS source code is available at several locations; in all of the following,
upstream `master` branch is kept up-to-date. Various development branches
are not necessarily pushed everywhere (and they should be considered
unstable in the sense that rebases and forced pushes are possible).

mirror                                          | note                    | provider                                          | backend
----------------------------------------------- | ----------------------- | ------------------------------------------------- | ------
<https://repo.or.cz/qpms.git>                   | primary public upstream | [repo.or.cz](https://repo.or.cz/)                 | girocco
<https://codeberg.org/QPMS/qpms>                |                         | [Codeberg](https://codeberg.org)                  | gitea
<https://git.piraattipuolue.fi/QPMS/qpms.git>   |                         | [Pirate Party Finland](https://piraattipuolue.fi) | gitea
<https://version.aalto.fi/gitlab/qpms/qpms.git> |                         | [Aalto University](https://aalto.fi)              | gitlab 
